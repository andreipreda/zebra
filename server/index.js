const express = require('express');

const publicweb = './zebra';
const app = express();

app.use(express.static(publicweb));
console.log(`Serving ${publicweb}`);
app.get('*', (_, res) => {
  res.sendFile(`index.html`, { root: publicweb });
});

const port = process.env.SERVER_PORT || '8080';
app.listen(port, () => console.log(`Zebra running on localhost:${port}`));

