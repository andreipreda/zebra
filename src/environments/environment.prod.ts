export const environment = {
  production: true,
  server_endpoint: 'https://dinosaur.azurewebsites.net',
  local_endpoint: 'https://dinosaur.azurewebsites.net'
};
