import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, tap, switchMap, withLatestFrom, filter, mapTo } from 'rxjs/operators';
import { environment } from './../environments/environment';
import * as zebActions from './zebras/actions/zebra.actions';
import * as zebraModels from './zebras/models/zebra';
import * as authActions from './auth/actions/auth.actions';
import { User } from './auth/models/user';
import * as fromAuth from './auth/reducers';

const FILE_KEY = 'csvfile';

export const server = environment.server_endpoint;
export const addEndpoint = `${server}/api/add`;
export const getAllEndpoint = `${server}/api/all`;
export const loginEndpoint = `${server}/auth/login`;
export const registerEndpoint = `${server}/auth/register`;
export const docEndpoint = (id: string) => `${server}/api/csv/&?id=${id}`;

@Injectable()
export class AppEffects {

  @Effect()
  login$ = this.actions$.pipe(
    ofType<authActions.Login>(authActions.AuthActionTypes.Login),
    map(action => action.payload),
    switchMap(payload => this.http$.post(loginEndpoint, payload).pipe(
      tap((user: User) => sessionStorage.setItem('zebraLogin', JSON.stringify(user))),
      map((user: User) => new authActions.LoginSuccess(user)),
      catchError(({ error }) => of(new authActions.LoginFailure(error.error)))
    ))
  );

  @Effect()
  signup$ = this.actions$.pipe(
    ofType<authActions.Signup>(authActions.AuthActionTypes.Signup),
    map(action => action.payload),
    switchMap(payload => this.http$.post(registerEndpoint, payload).pipe(
      tap((user: User) => sessionStorage.setItem('zebraLogin', JSON.stringify(user))),
      map((user: User) => new authActions.SignupSuccess(user)),
      catchError(({ error }) => of(new authActions.SignupFailure(error.error)))
    ))
  );

  @Effect()
  uploadFile$ = this.actions$.pipe(
    ofType<zebActions.Upload>(zebActions.ZebraActionTypes.Upload),
    map(action => action.payload),
    withLatestFrom(this.store.select(fromAuth.getUser)),
    filter(([file, user]) => user !== null),
    switchMap(([file, user]: [File, User]) => {

      const formData = new FormData();
      formData.append(FILE_KEY, new Blob([file]), file.name);

      return this.http$.post(
        addEndpoint,
        formData,
        { headers: new HttpHeaders({ 'Authorization': user.token })}
      ).pipe(
        map((doc: zebraModels.Doc) => new zebActions.UploadSuccess(doc)),
        catchError(({ error }) => of(new zebActions.FetchFailure(error.error)))
      );
    })
  );

  @Effect()
  updateLatest$ = this.actions$.pipe(
    ofType<zebActions.UploadSuccess | zebActions.DocRemoveSuccess>(
      zebActions.ZebraActionTypes.UploadSuccess,
      zebActions.ZebraActionTypes.DocRemoveSuccess
    ),
    withLatestFrom(this.store.select(fromAuth.getUser)),
    filter(([file, user]) => user !== null),
    map(([_, user]: [any, User]) => new zebActions.Docs(user.token))
  );

  @Effect()
  resetState$ = this.actions$.pipe(
    ofType<zebActions.DocsSuccess>(zebActions.ZebraActionTypes.DocsSuccess),
    filter(({ payload }: any) => payload.length === 0),
    mapTo(new zebActions.DocSelectedReset())
  );

  @Effect()
  getAll$ = this.actions$.pipe(
    ofType<zebActions.Docs>(zebActions.ZebraActionTypes.Docs),
    map(action => action.payload),
    switchMap((token: string) => this.http$.get(getAllEndpoint, {headers: new HttpHeaders({ 'Authorization': token })})
    .pipe(
        map((doc_list: zebraModels.DocSummary[]) => new zebActions.DocsSuccess(doc_list)),
        catchError(({ error }) => of(new zebActions.FetchFailure(error.error)))
    ))
  );

  @Effect()
  onDocRemove$ = this.actions$.pipe(
    ofType<zebActions.DocRemove>(zebActions.ZebraActionTypes.DocRemove),
    map(action => action.payload),
    withLatestFrom(this.store.select(fromAuth.getUser)),
    filter(([doc_id, user]) => user !== null),
    switchMap(([doc_id, user]: [string, User]) => this.http$.delete(docEndpoint(doc_id), {headers: new HttpHeaders({ 'Authorization': user.token })})
    .pipe(
        map((_ => new zebActions.DocRemoveSuccess()),
        catchError(({ error }) => of(new zebActions.FetchFailure(error.error)))
    ))
  ));

  @Effect()
  onDocSelected$ = this.actions$.pipe(
    ofType<zebActions.DocDetails>(zebActions.ZebraActionTypes.DocDetails),
    map(action => action.payload),
    withLatestFrom(this.store.select(fromAuth.getUser)),
    filter(([doc_id, user]) => user !== null),
    switchMap(([doc_id, user]: [string, User]) => this.http$.get(docEndpoint(doc_id), {headers: new HttpHeaders({ 'Authorization': user.token })})
    .pipe(
        map(((doc: zebraModels.Doc) => new zebActions.DocDetailsSuccess(doc)),
        catchError(({ error }) => of(new zebActions.FetchFailure(error.error)))
    ))
  ));

  @Effect()
  authSuccess$ = this.actions$.pipe(
    ofType<authActions.LoginSuccess | authActions.SignupSuccess>(
      authActions.AuthActionTypes.LoginSuccess,
      authActions.AuthActionTypes.SignupSuccess
    ),
    map(action => action.payload),
    // fetch the user's existing documents
    map((user: User) => new zebActions.Docs(user.token)),
    tap(() => this.router.navigate(['/']))
  );

  @Effect()
  $getAllDocsDashboard$ = this.actions$.pipe(
    ofType('ROUTER_NAVIGATION'),
    filter(({ payload }: any) => {
      const { routerState: { url } } = payload;
      console.log('URL ', url);
      return url === '/zebras';
    }),
    withLatestFrom(this.store.select(fromAuth.getUser)),
    // ensure the user's existing documents as the docs reducer it's load dynamically
    // and sometimes it's not there just after login
    map(([_, user]: [any, User]) => new zebActions.Docs(user.token))
  );

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(authActions.AuthActionTypes.LoginRedirect),
    tap(_ => {
      this.router.navigate(['/login']);
    })
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(authActions.AuthActionTypes.Logout),
    tap(_ => {
      const user = sessionStorage.getItem('zebraLogin');
      if (user) {
        sessionStorage.removeItem('zebraLogin');
      }
      this.router.navigate(['/login']);
    })
  );

  @Effect({ dispatch: false })
  signupRedirect$ = this.actions$.pipe(
    ofType(authActions.AuthActionTypes.SignupRedirect),
    tap(_ => {
      this.router.navigate(['/signup']);
    })
  );

  // Auth from session storage
  @Effect()
  onInit$ = this.actions$.pipe(
    ofType('ZEBRA_INIT'),
    map(_ => {
      const user = sessionStorage.getItem('zebraLogin');
      if (user) {
        return new authActions.LoginSuccess(JSON.parse(user) as User);
      } else {
        this.router.navigate(['/login']);
        return new authActions.LoginRedirect();
      }
    })
  );

  constructor(
    private actions$: Actions,
    private http$: HttpClient,
    private router: Router,
    private store: Store<fromAuth.AuthState>
  ) {}
}
