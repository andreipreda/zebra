import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { AppEffects } from './app.effects';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import * as zebActions from './zebras/actions/zebra.actions';
import * as authActions from './auth/actions/auth.actions';
import * as fromAuth from './auth/reducers';
import { User } from './auth/models/user';
import { concat, switchAll } from 'rxjs/operators';

describe('AppEffects', () => {
  let store: Store<fromAuth.State>;
  let effects: AppEffects;
  let actions$: Observable<any>;
  let routerService: any;
  let http$: HttpClient;
  let httpTestingCtrl: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({
          auth: combineReducers(fromAuth.reducers),
        }),
      ],
      providers: [
        AppEffects,
        {
          provide: HttpClient,
          useValue: {
            post: jest.fn(),
            get: jest.fn(),
            delete: jest.fn(),
            put: jest.fn()
          }
        },
        {
          provide: Store,
          useValue: {
            select: jest.fn()
          }
        },
        provideMockActions(() => actions$),
        {
          provide: Router,
          useValue: { navigate: jest.fn() },
        },
      ],
    });

    store = TestBed.get(Store);
    effects = TestBed.get(AppEffects);
    actions$ = TestBed.get(Actions);
    routerService = TestBed.get(Router);
    http$ = TestBed.get(HttpClient);
    httpTestingCtrl = TestBed.get(HttpTestingController);

    spyOn(routerService, 'navigate').and.callThrough();
    spyOn(fromAuth, 'getUser').and.returnValue({'token': '123'});
  });

  console.log = s => {
    process.stdout.write(s + '\n');
  };

  describe('getAll$', () => {
    it('should successfully fetch the doc list', () => {
      const allDocsReq = new zebActions.Docs('token_string');
      const resAction = new zebActions.DocsSuccess([{name: '1'}]);

      actions$ = hot('---r--', { r: allDocsReq });
      const response = cold('-q|', { q: [{name: '1'}] });
      const expected = cold('----e', { e: resAction });

      http$.get = jest.fn(() => response);

      expect(effects.getAll$).toBeObservable(expected);
    });
  });

  describe('resetState$', () => {
    it('should trigger a reset selected doc if a new doc uploaded successfully', () => {
      const action = new zebActions.DocsSuccess([]);
      const expectedAction = new zebActions.DocSelectedReset();

      actions$ = hot('--g---', { g: action });
      const expected = cold('--e', { e: expectedAction });

      expect(effects.resetState$).toBeObservable(expected);
    });
  });


  describe('login$', () => {
    it('should return an LoginSuccess action, with user information if login succeeds', () => {
      const credentials = { email: 'test@gmail.com', password: '123' };
      const user = { name: 'User', email: 'test@gmail.com' } as User;
      const action = new authActions.Login(credentials);
      const completion = new authActions.LoginSuccess(user);

      actions$ = hot('-a---', { a: action });
      const response = cold('-a|', { a: user });
      const expected = cold('--b', { b: completion });

      http$.post = jest.fn(() => response);

      expect(effects.login$).toBeObservable(expected);
    });


    it('Should concatenate two cold observables into single cold observable', () => {
      const a = cold('-a-|');
      const b = cold('-b-|');
      const expected = '-a--b-|';
      expect(a.pipe(concat(b))).toBeMarble(expected);
    });
  });

  describe('authSuccess$', () => {
    it('should dispatch a RouterNavigation action', () => {
      const user = { name: 'User', email: 'test@gmail.com' } as User;
      const action = new authActions.LoginSuccess(user);

      actions$ = hot('-a---', { a: action });

      effects.authSuccess$.subscribe(() => {
        expect(routerService.navigate).toHaveBeenCalledWith(['/']);
      });
    });
  });


  describe('loginRedirect$', () => {
    it('should dispatch a RouterNavigation action when auth.LoginRedirect is dispatched', () => {
      const action = new authActions.LoginRedirect();

      actions$ = hot('-a---', { a: action });

      effects.loginRedirect$.subscribe(() => {
        expect(routerService.navigate).toHaveBeenCalledWith(['/login']);
      });
    });

    it('should dispatch a RouterNavigation action when auth.Logout is dispatched', () => {
      const action = new authActions.Logout();

      actions$ = hot('-a---', { a: action });

      effects.loginRedirect$.subscribe(() => {
        expect(routerService.navigate).toHaveBeenCalledWith(['/login']);
      });
    });
  });
});
