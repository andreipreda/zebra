import { Action } from '@ngrx/store';
import { User, Authenticate, Register } from '../models/user';

export enum AuthActionTypes {
  Login = '[Auth] Login',
  Signup = '[Auth] Signup',
  Logout = '[Auth] Logout',
  LoginSuccess = '[Auth] Login Success',
  LoginFailure = '[Auth] Login Failure',
  LoginRedirect = '[Auth] Login Redirect',
  SignupSuccess = '[Auth] Signup Success',
  SignupFailure = '[Auth] Signup Failure',
  SignupRedirect = '[Auth] Signup Redirect'
}

export class Login implements Action {
  readonly type = AuthActionTypes.Login;

  constructor(public payload: Authenticate) {}
}

export class Signup implements Action {
  readonly type = AuthActionTypes.Signup;

  constructor(public payload: Register) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: User) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;

  constructor(public payload: string) {}
}

export class LoginRedirect implements Action {
  readonly type = AuthActionTypes.LoginRedirect;
}

export class SignupRedirect implements Action {
  readonly type = AuthActionTypes.SignupRedirect;
}

export class SignupFailure implements Action {
  readonly type = AuthActionTypes.SignupFailure;

  constructor(public payload: string) {}
}

export class SignupSuccess implements Action {
  readonly type = AuthActionTypes.SignupSuccess;

  constructor(public payload: User) {}
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export type AuthActionsUnion =
  | Login
  | Signup
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | SignupRedirect
  | SignupSuccess
  | SignupFailure
  | Logout;
