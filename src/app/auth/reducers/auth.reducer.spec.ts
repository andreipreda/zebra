import { reducer } from './auth.reducer';
import * as fromAuth from './auth.reducer';
import { Login, LoginSuccess, Logout } from '../actions/auth.actions';
import { Authenticate } from '../models/user';

describe('AuthReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);

      expect(result).toMatchSnapshot();
    });
  });

  describe('wrong login payload', () => {
    it('should NOT authenticate a user', () => {
      const user = { email: 'someUserName', password: 'test' } as Authenticate;
      const createAction = new Login(user);

      const expectedResult = fromAuth.initialState;

      const result = reducer(fromAuth.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('LOGIN_SUCCESS', () => {
    it('should add a user set loggedIn to true in auth state', () => {
      const user = { name: 'test', email: 'test@gmail.com' };
      const createAction = new LoginSuccess(user);

      const expectedResult = {
        loggedIn: true,
        user: { name: 'test' },
      };

      const result = reducer(fromAuth.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('LOGOUT', () => {
    it('should logout a user', () => {
      const initialState = {
        loggedIn: true,
        user: { name: 'test', email: 'test@gmail.com' },
      } as fromAuth.State;
      const createAction = new Logout();

      const expectedResult = fromAuth.initialState;

      const result = reducer(initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
});
