export interface Authenticate {
  email: string;
  password: string;
}

export interface Register {
  name: string;
  email: string;
  password: string;
}

export interface User {
  name: string;
  email: string;
  token?: string;
  _id?: string;
}
