import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Register } from '../models/user';

@Component({
  selector: 'zeb-signup-form',
  template: `
    <mat-card>
      <mat-card-title>Register a new account</mat-card-title>
      <mat-card-content>
        <form [formGroup]="form" (ngSubmit)="submit()">
          <p>
            <mat-form-field>
              <input type="text" required matInput placeholder="Name" formControlName="name">
            </mat-form-field>
          </p>

          <p>
            <mat-form-field>
              <input type="text" required matInput placeholder="Email" formControlName="email">
            </mat-form-field>
          </p>

          <p>
            <mat-form-field>
              <input type="password" required matInput placeholder="Password" formControlName="password">
            </mat-form-field>
          </p>

          <p *ngIf="errorMessage" class="registerError">
            {{ errorMessage }}
          </p>

          <p class="registerButtons">
            <button type="submit" mat-button>Signup</button>
          </p>

        </form>
      </mat-card-content>
    </mat-card>
  `,
  styles: [
    `
    :host {
      display: flex;
      justify-content: center;
      margin: 72px 0;
    }

    .mat-form-field {
      width: 100%;
      min-width: 300px;
    }

    mat-card-title,
    mat-card-content {
      display: flex;
      justify-content: center;
    }

    .registerError {
      padding: 16px;
      width: 300px;
      color: white;
      background-color: red;
    }

    .registerButtons {
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
    }
  `,
  ],
})
export class SignupFormComponent implements OnInit {
  @Input()
  set pending(isPending: boolean) {
    if (isPending) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  @Input() errorMessage: string | null;

  @Output() submitted = new EventEmitter<Register>();

  form = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor() {}

  ngOnInit() {}

  submit() {
    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }
}
