import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MatInputModule, MatCardModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule, Store, combineReducers } from '@ngrx/store';
import { SignupPageComponent } from './signup-page.component';
import { SignupFormComponent } from '../components/signup-form.component';
import * as AuthActions from '../actions/auth.actions';
import * as fromAuth from '../reducers';

describe('Signup Page', () => {
  let fixture: ComponentFixture<SignupPageComponent>;
  let store: Store<fromAuth.State>;
  let instance: SignupPageComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        StoreModule.forRoot({
          auth: combineReducers(fromAuth.reducers),
        }),
        MatInputModule,
        MatCardModule,
        ReactiveFormsModule,
      ],
      declarations: [SignupPageComponent, SignupFormComponent],
    });

    fixture = TestBed.createComponent(SignupPageComponent);
    instance = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should compile', () => {
    fixture.detectChanges();

    expect(fixture).toMatchSnapshot();
  });

  it('should dispatch a signup event on submit', () => {
    const $event: any = {};
    const action = new AuthActions.Signup($event);

    instance.onSubmit($event);

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
