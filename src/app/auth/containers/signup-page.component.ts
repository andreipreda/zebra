import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Register } from '../models/user';
import * as fromAuth from '../reducers';
import * as AuthActions from '../actions/auth.actions';

@Component({
  selector: 'zeb-signup-page',
  template: `
    <zeb-signup-form
      (submitted)="onSubmit($event)"
      [pending]="pending$ | async"
      [errorMessage]="error$ | async"
    >
    </zeb-signup-form>
  `,
  styles: [],
})
export class SignupPageComponent implements OnInit {
  pending$ = this.store.pipe(select(fromAuth.getSignupPagePending));
  error$ = this.store.pipe(select(fromAuth.getSignupPageError));

  constructor(private store: Store<fromAuth.State>) {}

  ngOnInit() {}

  onSubmit($event: Register) {
    this.store.dispatch(new AuthActions.Signup($event));
  }
}
