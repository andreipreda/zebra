import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Authenticate } from '../models/user';
import * as fromAuth from '../reducers';
import * as AuthActions from '../actions/auth.actions';

@Component({
  selector: 'zeb-login-page',
  template: `
    <zeb-login-form
      (submitted)="onSubmit($event)"
      (newAccountReq)="onSignupReq($event)"
      [pending]="pending$ | async"
      [errorMessage]="error$ | async"
    >
    </zeb-login-form>
  `,
  styles: [],
})
export class LoginPageComponent implements OnInit {
  pending$ = this.store.pipe(select(fromAuth.getLoginPagePending));
  error$ = this.store.pipe(select(fromAuth.getLoginPageError));

  constructor(private store: Store<fromAuth.State>) {}

  ngOnInit() {}

  onSubmit($event: Authenticate) {
    this.store.dispatch(new AuthActions.Login($event));
  }

  onSignupReq($event: boolean) {
    console.log('acc req fom container');
    this.store.dispatch(new AuthActions.SignupRedirect());
  }
}
