export interface UploadFile {
  file: string;
}

export interface DocSummary {
  name?: string;
  created_at?: Date;
  id_?: string;
}

export interface Doc extends DocSummary {
  content: any[];
}

export interface DashboardState {
  doc_selected: Doc;
}

export interface DocListSelected {
  id: string;
  selected: boolean;
}
