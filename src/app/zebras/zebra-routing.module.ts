import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// containers
import { DashboardPageComponent } from './containers/dashboard-page.component';

export const routes: Routes = [
  { path: '', component: DashboardPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZebrasRoutingModule {}
