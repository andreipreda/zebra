import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as zebActions from './../actions/zebra.actions';
import { UploadFile, DocListSelected } from './../models/zebra';
import * as fromZebra from './../reducers';
import { filter, pluck } from 'rxjs/operators';

@Component({
  selector: 'zeb-dashboard-page',
  template: `
  <mat-card>
    <mat-card-title class="dash-top">
      <zeb-upload-csv
        (submitted)="onSubmit($event)"
        [errorMessage]="error$ | async"
        [pendingReq]="pending$ | async"
      >
      </zeb-upload-csv>
      <zeb-file-list
        [docs]="docs$ | async"
        [selectedDoc]="selected$ | async"
        (removeDoc)="onRemoveDoc($event)"
        (selectDoc)="onDocSelected($event)"
      ></zeb-file-list>
    </mat-card-title>
    <mat-card-content>
      <zeb-chart-list
        [charts]="charts$ | async"
      ></zeb-chart-list>
    </mat-card-content>
  </mat-card>
  `,
  styles: [
    `
    .dash-top {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      flex-wrap: wrap;
    }
  `,
  ]
})
export class DashboardPageComponent {
  docs$ = this.store.pipe(select(fromZebra.getDocList));
  error$ = this.store.pipe(select(fromZebra.getError));
  pending$ = this.store.pipe(select(fromZebra.getPending));
  charts$ = this.store.pipe(select(fromZebra.getSelectedDoc), filter(Boolean), pluck('content'));
  selected$ = this.store.pipe(select(fromZebra.getSelectedDoc), filter(Boolean), pluck('_id'));

  constructor(private store: Store<any>) {}

  onSubmit($event: UploadFile) {
    this.store.dispatch(new zebActions.Upload($event));
  }

  onRemoveDoc($event: DocListSelected) {
    const { id, selected } = $event;
    this.store.dispatch(new zebActions.DocRemove(id));
    if (selected) {
      this.store.dispatch(new zebActions.DocSelectedReset());
    }
  }

  onDocSelected($event: string) {
    this.store.dispatch(new zebActions.DocDetails($event));
  }
}
