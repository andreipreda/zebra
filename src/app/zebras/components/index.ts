import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { UploadCsvComponent } from './upload-csv.component';
import { FileListComponent } from './file-list.component';
import { ChartListComponent } from './chart-list.component';
import { ChartComponent } from './chart.component';

import { MaterialModule } from '../../material';

export const COMPONENTS = [
  UploadCsvComponent,
  FileListComponent,
  ChartListComponent,
  ChartComponent
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class ComponentsModule {}
