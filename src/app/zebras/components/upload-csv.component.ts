import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'zeb-upload-csv',
  template: `
    <form [formGroup]="form" (ngSubmit)="submit()">
      <mat-label for="file">Choose File</mat-label>
      <br />
      <input type="file" formControlName="file" (change)="handleFileInput($event.target.files)" required id="file">
      <p>
        <button color="primary" type="submit" mat-raised-button [disabled]="form.invalid || form.pristine || pendingReq">Upload</button>
      </p>
    </form>
    <mat-spinner *ngIf="pendingReq" diameter="25" strokeWidth="8"></mat-spinner>
    <p *ngIf="errorMessage" class="dashboardError">
        {{ errorMessage }}
    </p>
  `,
  styles: [`
    .dashboardError {
      padding: 16px;
      font-size: 14px;
      width: 230px;
      color: white;
      background-color: red;
    }
  `],
})
export class UploadCsvComponent {
  @Input() errorMessage: string | null;
  @Input() pendingReq: boolean;
  @Output() submitted = new EventEmitter<File>();

  fileToUpload: File = null;
  form = new FormGroup({
    file: new FormControl([''])
  });

  // Only handles one file at a time!
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  submit() {
    if (this.form.valid) {
      this.submitted.emit(this.fileToUpload);
    }
  }

  constructor() {}
}
