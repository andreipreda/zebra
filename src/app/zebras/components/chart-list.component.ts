import { Component, Input } from '@angular/core';
import { ChartData } from 'chart.js';

@Component({
  selector: 'zeb-chart-list',
  template: `
  <mat-grid-list cols='2' rowHeight='2:1' *ngIf="charts.length > 0">
    <mat-grid-tile colspan='2' *ngFor="let chart of charts">
      <zeb-chart [chartData]="chart"></zeb-chart>
    </mat-grid-tile>
  </mat-grid-list>
  `,
})
export class ChartListComponent {
  @Input() charts: ChartData[];

  constructor() {}
}
