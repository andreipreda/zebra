import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DocSummary, DocListSelected } from './../models/zebra';

@Component({
  selector: 'zeb-file-list',
  template: `
  <mat-list *ngIf="docs.length > 0" class="doc-list-container">
    <h3 mat-subheader>Uploaded Files ({{docs.length}})</h3>
    <mat-list-item
      class="list-item"
      [class.doc-active]="doc._id === selectedDoc"
      *ngFor="let doc of docs"
    >
      <mat-icon mat-list-icon>insert_drive_file</mat-icon>
      <h4 mat-line>{{doc.name}}</h4>
      <p mat-line>Added on {{doc.created_at | date:'d MMM yy  HH:mm'}}</p>
      <button
        mat-icon-button
        color="accent"
        (click)="handleDocSelected(doc._id)"
      >
        <mat-icon aria-label="Show Chart">insert_chart</mat-icon>
      </button>
      <button
        mat-icon-button
        color="accent"
        (click)="handleDocRemove(doc._id)"
      >
      <mat-icon aria-label="Delete Forever">delete_forever</mat-icon>
      </button>
    </mat-list-item>
  </mat-list>
  `,
  styles: [`
    .doc-list-container {
      max-height: 250px;
      min-width: 700px;
      overflow: auto;
    }
    .list-item {
      border-bottom: 1px solid #525252;
    }
    .doc-active {
      color: pink;
      font-weight: bold;
    }
  `]
})
// <p *ngIf="docs.length === 0">No files uploaded yet!</p>
export class FileListComponent {
  @Input() docs: DocSummary[];
  @Input() selectedDoc: string | null;
  @Output() removeDoc = new EventEmitter<DocListSelected>();
  @Output() selectDoc = new EventEmitter<string>();

  handleDocSelected(id: string) {
    this.selectDoc.emit(id);
  }

  handleDocRemove(id: string) {
    this.removeDoc.emit({ id, selected: this.selectedDoc === id });
  }
}
