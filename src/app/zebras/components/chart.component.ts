import { Component, OnInit, ElementRef, ViewChild, OnDestroy, Input } from '@angular/core';
import { Chart, ChartData } from 'chart.js';

@Component({
  selector: 'zeb-chart',
  template: `
    <canvas width="1200px" height="300px" #chartCanvas>{{ chart }}</canvas>
  `,
})
export class ChartComponent implements OnInit, OnDestroy {
  @ViewChild('chartCanvas') ctx;
  @Input() chartData: ChartData;

  chart = null;

  ngOnInit() {
    this.createChart();
  }

  ngOnDestroy() {
    if (this.chart) {
      this.chart.destroy();
    }
  }

  private chartType (labels: (string | string[])[]) {

    const len = labels.length;

    switch (true) {
      case (len < 4): return 'doughnut';
      case (len > 20): {
        this.ctx.nativeElement.width = 1200;
        return 'bar';
      }
      default: return 'line';
    }
  }

  createChart () {
    const data = JSON.parse(JSON.stringify(this.chartData));
    const chartData = {
      type: this.chartType(this.chartData.labels),
      data,
      options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            yAxes: [{
              // scaleLabel: {
              //   display: true
              // }
            }],
          title: {
            display: true,
            text: data.datasets[0].label
          }
        }
      }
    };

    this.chart = new Chart(this.ctx.nativeElement, chartData);
  }

  constructor(private elementRef: ElementRef) {}
}
