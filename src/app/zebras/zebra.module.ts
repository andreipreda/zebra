import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { FileInputAccessorModule } from 'file-input-accessor';

import { ComponentsModule } from './components';
import { MaterialModule } from '../material';
import { ZebrasRoutingModule } from './zebra-routing.module';
import { DashboardPageComponent } from './containers/dashboard-page.component';
import { reducers } from './reducers';

export const COMPONENTS = [
  DashboardPageComponent
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ComponentsModule,
    ZebrasRoutingModule,
    FileInputAccessorModule,
    StoreModule.forFeature('dashboard', reducers),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [],
})
export class ZebrasModule {}
