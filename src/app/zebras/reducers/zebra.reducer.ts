import * as model from './../models/zebra';
import { ZebraActionsUnion, ZebraActionTypes } from './../actions/zebra.actions';
import { ChartData } from 'chart.js';
import randomColor from 'randomcolor';

export interface State {
  doc_list: model.DocSummary[];
  selected: model.Doc | null;
  error: string;
  pending: boolean;
}

type Calculation = 'sum' | 'average';

interface MongoChartData {
  labels: [string, string, Calculation];
  data: [string, number][];
}

const initialState: State = {
  doc_list: [],
  selected: {
    content: []
  },
  error: '',
  pending: false
};

export function reducer(state = initialState, action: ZebraActionsUnion): State {
  switch (action.type) {
    case ZebraActionTypes.FetchFailure: {
      const error = action.payload;

      return {
        ...state,
        error: error,
        pending: false
      };
    }

    case ZebraActionTypes.DocsSuccess: {

      const docs = action.payload;
      return {
        ...state,
        error: '',
        doc_list: docs
      };
    }

    case ZebraActionTypes.Upload: {
      return {
        ...state,
        pending: true
      };
    }

    case ZebraActionTypes.DocDetailsSuccess:
    case ZebraActionTypes.UploadSuccess: {

      const doc = action.payload;

      const { content } = doc;
      console.log('content', content);

      const createChart = (mChart: MongoChartData): ChartData => {
        const [pivot, by, operation] = mChart.labels;
        const len = mChart.labels.length;
        return {
          labels: mChart.data.map(([l, _]) => l),
          datasets: [{
            label: `The ${operation} of ${by.toLowerCase()} grouped by ${pivot.toLowerCase()}`,
            data: mChart.data.map(([_, d]) => d),
            backgroundColor: mChart.data.map(_ => randomColor()),
            borderColor: len < 20 && len > 4 ? '#525252' : randomColor(),
            fill: len < 20 && len > 4,
            borderWidth: len < 20 || len > 4 ? 0 : 1
          }]
        };
      };

      const newCharts = content.map(createChart);

      const processedDoc = {
        ...doc,
        content: newCharts
      };

      return {
        ...state,
        error: '',
        selected: processedDoc,
        pending: false
      };
    }

    case ZebraActionTypes.DocSelectedReset: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getSelectedDoc = (state: State) => state.selected;
export const getDocList = (state: State) => state.doc_list;
export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
