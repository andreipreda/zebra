import {
  createSelector,
  createFeatureSelector,
  ActionReducerMap,
} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromZebra from './zebra.reducer';

export interface ZebraState {
  dashboard: fromZebra.State;
}

export interface State extends fromRoot.State {
  dashboard: fromZebra.State;
}

export const reducers: ActionReducerMap<ZebraState> = {
  dashboard: fromZebra.reducer
};

export const selectDashboardState = createFeatureSelector<ZebraState>('dashboard');

export const selectZebraState = createSelector(
  selectDashboardState,
  (state: ZebraState) => state.dashboard
);

export const getSelectedDoc = createSelector(
  selectZebraState,
  fromZebra.getSelectedDoc
);

export const getError = createSelector(
  selectZebraState,
  fromZebra.getError
);

export const getDocList = createSelector(
  selectZebraState,
  fromZebra.getDocList
);

export const getPending = createSelector(
  selectZebraState,
  fromZebra.getPending
);
