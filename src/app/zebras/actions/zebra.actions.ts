import { Action } from '@ngrx/store';
import * as models from '../models/zebra';

export enum ZebraActionTypes {
  Upload = '[Zebra] Upload',
  UploadSuccess = '[Zebra] UploadSuccess',
  FetchFailure = '[Zebra] FetchFailure',
  Docs = '[Zebra] Docs',
  DocsSuccess = '[Zebra] DocsSuccess',
  DocRemove = '[Zebra] DocRemove',
  DocRemoveSuccess = '[Zebra] DocRemoveSuccess',
  DocDetails = '[Zebra] DocDetails',
  DocDetailsSuccess = '[Zebra] DocDetailsSuccess',
  DocSelectedReset = '[Zebra] DocSelectedReset'
}

export class Upload implements Action {
  readonly type = ZebraActionTypes.Upload;

  constructor(public payload: models.UploadFile) {}
}

export class UploadSuccess implements Action {
  readonly type = ZebraActionTypes.UploadSuccess;

  constructor(public payload: models.Doc) {}
}

export class FetchFailure implements Action {
  readonly type = ZebraActionTypes.FetchFailure;

  constructor(public payload: string) {}
}

export class Docs implements Action {
  readonly type = ZebraActionTypes.Docs;

  constructor(public payload: string) {}
}

export class DocDetails implements Action {
  readonly type = ZebraActionTypes.DocDetails;

  constructor(public payload: string) {}
}

export class DocDetailsSuccess implements Action {
  readonly type = ZebraActionTypes.DocDetailsSuccess;

  constructor(public payload: models.Doc) {}
}

export class DocsSuccess implements Action {
  readonly type = ZebraActionTypes.DocsSuccess;

  constructor(public payload: models.DocSummary[]) {}
}

export class DocRemove implements Action {
  readonly type = ZebraActionTypes.DocRemove;

  constructor(public payload: string) {}
}

export class DocRemoveSuccess implements Action {
  readonly type = ZebraActionTypes.DocRemoveSuccess;
}

export class DocSelectedReset implements Action {
  readonly type = ZebraActionTypes.DocSelectedReset;
}

export type ZebraActionsUnion =
  | Upload
  | UploadSuccess
  | FetchFailure
  | Docs
  | DocsSuccess
  | DocRemove
  | DocRemoveSuccess
  | DocDetails
  | DocDetailsSuccess
  | DocSelectedReset;
