import { ChangeDetectionStrategy, Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as AuthActions from '../../auth/actions/auth.actions';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import * as LayoutActions from '../actions/layout.actions';
import { User } from './../../auth/models/user';

@Component({
  selector: 'zeb-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <zeb-layout>
      <zeb-sidenav [open]="showSidenav$ | async">
        <zeb-nav-item (navigate)="closeSidenav()" *ngIf="!(loggedIn$ | async)">
          Sign In
        </zeb-nav-item>
        <zeb-nav-item (navigate)="logout()" *ngIf="loggedIn$ | async">
          Sign Out
        </zeb-nav-item>
        <zeb-nav-item (navigate)="closeSidenav()" *ngIf="loggedIn$ | async">
          Back to Dashboard
        </zeb-nav-item>
      </zeb-sidenav>
      <zeb-toolbar (openMenu)="openSidenav()">
        <div class="toolbar-content">
          <p>Zebra csv parser</p>
          <button mat-button>{{ userName$ | async }}</button>
        </div>
      </zeb-toolbar>

      <router-outlet></router-outlet>
    </zeb-layout>
  `,
  styles: [`
    .toolbar-content {
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    },
    user-name {
      font-size: 16px;
    }
  `]
})
export class AppComponent {
  showSidenav$: Observable<boolean>;
  loggedIn$: Observable<boolean>;
  userName$: Observable<User>;

  constructor(private store: Store<fromRoot.State>) {

    this.showSidenav$ = this.store.pipe(select(fromRoot.getShowSidenav));
    this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
    this.userName$ = this.store.pipe(
      select(fromAuth.getUser),
      map((user: any) => user ? user.name : null));
    this.store.dispatch({ type: 'ZEBRA_INIT' });
  }

  closeSidenav() {
    this.store.dispatch(new LayoutActions.CloseSidenav());
  }

  openSidenav() {
    this.store.dispatch(new LayoutActions.OpenSidenav());
  }

  logout() {
    this.closeSidenav();

    this.store.dispatch(new AuthActions.Logout());
  }
}
