# Zebra

Angular 6 UI for parsing a CSV and displaying some random charts based on the data.

Using redux architecture thanks to ngrx platform mainly store and effects (for controlling the flow of actions and handling http requests side effects)

The project was generated with [Angular CLI](https://github.com/angular/angular-cli)

## Run locally 

Run `npm install` or `yarn` if the case.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Run Unit Test

It uses Jest as a test framework using this setup [jest-preset-angular](https://github.com/thymikee/jest-preset-angular)

Run `npx jest` to execute the unit tests. Or `yarn jest` if preffered and yarn it's installed. 


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
